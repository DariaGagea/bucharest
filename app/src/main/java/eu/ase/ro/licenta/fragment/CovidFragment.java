package eu.ase.ro.licenta.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import eu.ase.ro.licenta.R;

public class CovidFragment extends Fragment {

    private TextView tv;

    public CovidFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_covid, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(final View view) {
        tv = view.findViewById(R.id.fragment_covid_tv_text2);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateTo(getString(R.string.fragment_covid_link));
            }
        });
    }

    private void navigateTo(String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
