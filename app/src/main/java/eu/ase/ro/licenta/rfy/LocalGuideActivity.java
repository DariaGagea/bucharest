package eu.ase.ro.licenta.rfy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.licenta.MainActivity;
import eu.ase.ro.licenta.QuizActivity;
import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.firebase.FirebaseController;
import eu.ase.ro.licenta.firebase.FirebaseNotifier;
import eu.ase.ro.licenta.firebase.Recommendation;

public class LocalGuideActivity extends AppCompatActivity {
    public static final String DRAWABLE = "@drawable/";

    private ImageView iv1;
    private ImageView iv2;

    private TextView tv1;
    private TextView tv2;

    private TextView tvText;

    private List<Recommendation> recommendations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_guide);
        initComponents();
        recommendations = MainActivity.recommendationMap.get(MainActivity.answer11LocalGuide);
        initFields();
    }

    private void initComponents() {
        iv1 = findViewById(R.id.activity_localGuide_iv_1);
        iv2 = findViewById(R.id.activity_localGuide_iv_2);

        tv1 = findViewById(R.id.activity_localGuide_tv_1);
        tv2 = findViewById(R.id.activity_localGuide_tv_2);

        tvText = findViewById(R.id.activity_local_guide_tv_text);
    }

    private void initFields() {
        if (MainActivity.answer11LocalGuide.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_localGuideVar1))) {
            tvText.setText(R.string.activity_local_guide_tv_var1);
        }

        if (MainActivity.answer11LocalGuide.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_localGuideVar2))) {
            tvText.setText(R.string.activity_local_guide_tv_var2);
        }

        tv1.setText(recommendations.get(0).getName());
        iv1.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(0).getImageId(), null, getPackageName()));
        iv1.setOnClickListener(v -> navigateTo(v, recommendations.get(0).getLink()));

        tv2.setText(recommendations.get(1).getName());
        iv2.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(1).getImageId(), null, getPackageName()));
        iv2.setOnClickListener(v -> navigateTo(v, recommendations.get(1).getLink()));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
