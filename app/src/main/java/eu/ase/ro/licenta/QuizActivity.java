package eu.ase.ro.licenta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import eu.ase.ro.licenta.R;

import static eu.ase.ro.licenta.WelcomeActivity.SHARED_PREF_NAME;

public class QuizActivity extends AppCompatActivity {
    private Button btnFinish;

    private RadioButton rb1_1;
    private RadioButton rb1_2;
    private RadioButton rb1_3;
    private RadioButton rb1_4;

    private RadioGroup rg2;
    private RadioGroup rg3;
    private RadioGroup rg4;
    private RadioGroup rg5;
    private RadioGroup rg6;
    private RadioGroup rg7;
    private RadioGroup rg8;
    private RadioGroup rg9;
    private RadioGroup rg10;
    private RadioGroup rg11;

    private String answer1NoDays;
    private String answer2FoodAndDrinks;
    private String answer3Outdoor;
    private String answer4Museums;
    private String answer5CoffeeAndBars;
    private String answer6SportsAndSpa;
    private String answer7Shopping;
    private String answer8Casino;
    private String answer9Nightlife;
    private String answer10Transport;
    private String answer11LocalGuide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        initComponents();
    }

    private void initComponents() {
        final RadioGroup rg1_1 = findViewById(R.id.activity_quiz_rg_options_1a);
        final RadioGroup rg1_2 = findViewById(R.id.activity_quiz_rg_options_1b);

        rb1_1 = findViewById(R.id.activity_quiz_rb_option_1_1);
        rb1_2 = findViewById(R.id.activity_quiz_rb_option_1_2);
        rb1_3 = findViewById(R.id.activity_quiz_rb_option_1_3);
        rb1_4 = findViewById(R.id.activity_quiz_rb_option_1_4);

        rb1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rg1_2.clearCheck();
            }
        });

        rb1_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rg1_2.clearCheck();
            }
        });

        rb1_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rg1_1.clearCheck();
            }
        });

        rb1_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rg1_1.clearCheck();
            }
        });

        rg2 = findViewById(R.id.activity_quiz_rg_options_2);
        rg3 = findViewById(R.id.activity_quiz_rg_options_3);
        rg4 = findViewById(R.id.activity_quiz_rg_options_4);
        rg5 = findViewById(R.id.activity_quiz_rg_options_5);
        rg6 = findViewById(R.id.activity_quiz_rg_options_6);
        rg7 = findViewById(R.id.activity_quiz_rg_options_7);
        rg8 = findViewById(R.id.activity_quiz_rg_options_8);
        rg9 = findViewById(R.id.activity_quiz_rg_options_9);
        rg10 = findViewById(R.id.activity_quiz_rg_options_10);
        rg11 = findViewById(R.id.activity_quiz_rg_options_11);

        btnFinish = findViewById(R.id.activity_quiz_leisure_btn_finish);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    saveAnswers();
                    writeInSharedPrefs();
                    Intent intentIntermmediate = new Intent(getApplicationContext(), IntermmediateActivity.class);
                    startActivity(intentIntermmediate);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.activity_quiz_message, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean validate() {
        if (validateQ1() && validateQ2() && validateQ3() && validateQ4() && validateQ5() && validateQ6() && validateQ7() && validateQ8() && validateQ9() && validateQ10() && validateQ11()) {
            return true;
        }
        return false;
    }

    private boolean validateQ1() {
        if (rb1_1.isChecked() || rb1_2.isChecked() || rb1_3.isChecked() || rb1_4.isChecked()) {
            return true;
        }
        return false;
    }

    private boolean validateQ2() {
        if (rg2.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ3() {
        if (rg3.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ4() {
        if (rg4.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ5() {
        if (rg5.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ6() {
        if (rg6.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ7() {
        if (rg7.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ8() {
        if (rg8.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ9() {
        if (rg9.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ10() {
        if (rg10.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private boolean validateQ11() {
        if (rg11.getCheckedRadioButtonId() != -1) {
            return true;
        }
        return false;
    }

    private void saveAnswers() {
        RadioButton rbans2 = findViewById(rg2.getCheckedRadioButtonId());
        answer2FoodAndDrinks = processAnswer2(rbans2.getText().toString());

        RadioButton rbans3 = findViewById(rg3.getCheckedRadioButtonId());
        answer3Outdoor = processAnswer3(rbans3.getText().toString());

        RadioButton rbans4 = findViewById(rg4.getCheckedRadioButtonId());
        answer4Museums = processAnswer4(rbans4.getText().toString());

        RadioButton rbans5 = findViewById(rg5.getCheckedRadioButtonId());
        answer5CoffeeAndBars = processAnswer5(rbans5.getText().toString());

        RadioButton rbans6 = findViewById(rg6.getCheckedRadioButtonId());
        answer6SportsAndSpa = processAnswer6(rbans6.getText().toString());

        RadioButton rbans7 = findViewById(rg7.getCheckedRadioButtonId());
        answer7Shopping = processAnswer7(rbans7.getText().toString());

        RadioButton rbans8 = findViewById(rg8.getCheckedRadioButtonId());
        answer8Casino = processAnswer8(rbans8.getText().toString());

        RadioButton rbans9 = findViewById(rg9.getCheckedRadioButtonId());
        answer9Nightlife = processAnswer9(rbans9.getText().toString());

        RadioButton rbans10 = findViewById(rg10.getCheckedRadioButtonId());
        answer10Transport = processAnswer10(rbans10.getText().toString());

        RadioButton rbans11 = findViewById(rg11.getCheckedRadioButtonId());
        answer11LocalGuide = processAnswer11(rbans11.getText().toString());
    }

    private String processAnswer2(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q2_ans1))) {
            return getString(R.string.activity_quiz_foodAndDrinkVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q2_ans2))) {
            return getString(R.string.activity_quiz_foodAndDrinkVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q2_ans3))) {
            return getString(R.string.activity_quiz_foodAndDrinkVar3);
        } else {
            return getString(R.string.activity_quiz_foodAndDrinkVar4);
        }
    }

    private String processAnswer3(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q3_ans1))) {
            return getString(R.string.activity_quiz_outdoorVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q3_ans2))) {
            return getString(R.string.activity_quiz_outdoorVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q3_ans3))) {
            return getString(R.string.activity_quiz_outdoorVar3);
        } else {
            return getString(R.string.activity_quiz_outdoorVar4);
        }
    }

    private String processAnswer4(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q4_ans1))) {
            return getString(R.string.activity_quiz_museumsVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q4_ans2))) {
            return getString(R.string.activity_quiz_museumsVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q4_ans3))) {
            return getString(R.string.activity_quiz_museumsVar3);
        } else {
            return getString(R.string.activity_quiz_museumsVar4);
        }
    }

    private String processAnswer5(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q5_ans1))) {
            return getString(R.string.activity_quiz_coffeeAndBarsVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q5_ans2))) {
            return getString(R.string.activity_quiz_coffeeAndBarsVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q5_ans3))) {
            return getString(R.string.activity_quiz_coffeeAndBarsVar3);
        } else {
            return getString(R.string.activity_quiz_coffeeAndBarsVar4);
        }
    }

    private String processAnswer6(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q6_ans1))) {
            return getString(R.string.activity_quiz_sportsAndSpaVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q6_ans2))) {
            return getString(R.string.activity_quiz_sportsAndSpaVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q6_ans3))) {
            return getString(R.string.activity_quiz_sportsAndSpaVar3);
        } else {
            return getString(R.string.activity_quiz_sportsAndSpaVar4);
        }
    }

    private String processAnswer7(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q7_ans1))) {
            return getString(R.string.activity_quiz_shoppingVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q7_ans2))) {
            return getString(R.string.activity_quiz_shoppingVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q7_ans3))) {
            return getString(R.string.activity_quiz_shoppingVar3);
        } else {
            return getString(R.string.activity_quiz_shoppingVar4);
        }
    }

    private String processAnswer8(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q8_ans1))) {
            return getString(R.string.activity_quiz_casinoVar1);
        } else {
            return getString(R.string.activity_quiz_casinoVar2);
        }
    }

    private String processAnswer9(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q9_ans1))) {
            return getString(R.string.activity_quiz_nightlifeVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q9_ans2))) {
            return getString(R.string.activity_quiz_nightlifeVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q9_ans3))) {
            return getString(R.string.activity_quiz_nightlifeVar3);
        } else {
            return getString(R.string.activity_quiz_nightlifeVar4);
        }
    }

    private String processAnswer10(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q10_ans1))) {
            return getString(R.string.activity_quiz_transportVar1);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q10_ans2))) {
            return getString(R.string.activity_quiz_transportVar2);
        } else if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q10_ans3))) {
            return getString(R.string.activity_quiz_transportVar3);
        } else {
            return getString(R.string.activity_quiz_transportVar4);
        }
    }

    private String processAnswer11(String answer) {
        if (answer.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_Q11_ans1))) {
            return getString(R.string.activity_quiz_localGuideVar1);
        } else {
            return getString(R.string.activity_quiz_localGuideVar2);
        }
    }

    private void writeInSharedPrefs() {
        SharedPreferences.Editor editor = WelcomeActivity.preferences.edit();
        editor.putString("ANS2", answer2FoodAndDrinks);
        editor.putString("ANS3", answer3Outdoor);
        editor.putString("ANS4", answer4Museums);
        editor.putString("ANS5", answer5CoffeeAndBars);
        editor.putString("ANS6", answer6SportsAndSpa);
        editor.putString("ANS7", answer7Shopping);
        editor.putString("ANS8", answer8Casino);
        editor.putString("ANS9", answer9Nightlife);
        editor.putString("ANS10", answer10Transport);
        editor.putString("ANS11", answer11LocalGuide);
        editor.apply();
    }
}