package eu.ase.ro.licenta;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.firebase.FirebaseController;
import eu.ase.ro.licenta.firebase.FirebaseNotifier;
import eu.ase.ro.licenta.firebase.Recommendation;
import eu.ase.ro.licenta.fragment.BusinessFragment;
import eu.ase.ro.licenta.fragment.CommunismFragment;
import eu.ase.ro.licenta.fragment.CovidFragment;
import eu.ase.ro.licenta.fragment.RecommendedForYouFragment;
import eu.ase.ro.licenta.fragment.GoodToKnowFragment;
import eu.ase.ro.licenta.fragment.RomaniaFragment;

public class MainActivity extends AppCompatActivity implements FirebaseNotifier {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Fragment currentFragment;

    private FirebaseController firebaseController;

    public static Map<String, List<Recommendation>> recommendationMap = new HashMap<>();

    private int itemId;

    public static String answer1NoDays;
    public static String answer2FoodAndDrinks;
    public static String answer3Outdoor;
    public static String answer4Museums;
    public static String answer5CoffeeAndBars;
    public static String answer6SportsAndSpa;
    public static String answer7Shopping;
    public static String answer8Casino;
    public static String answer9Nightlife;
    public static String answer10Transport;
    public static String answer11LocalGuide;

    private boolean isBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        readSharedPrefs();
        initComponents();
        firebaseController = FirebaseController.getInstance(this);
        firebaseController.find();
        openDefaultFragment(savedInstanceState);
    }

    private void openDefaultFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            currentFragment = new RecommendedForYouFragment();
            openFragment();
            navigationView.setCheckedItem(R.id.main_nav_RFY);
        }
    }

    private void initComponents() {
        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.main_nav_RFY) {
                    currentFragment = new RecommendedForYouFragment();
                } else if (menuItem.getItemId() == R.id.main_nav_GTK) {
                    currentFragment = new GoodToKnowFragment();
                } else if (menuItem.getItemId() == R.id.main_nav_communism) {
                    currentFragment = new CommunismFragment();
                } else if (menuItem.getItemId() == R.id.main_nav_jewish) {
                    navigateTo(getString(R.string.jewish_link));
                } else if (menuItem.getItemId() == R.id.main_nav_business) {
                    currentFragment = new BusinessFragment();
                } else if (menuItem.getItemId() == R.id.main_nav_romania) {
                    currentFragment = new RomaniaFragment();
                } else if (menuItem.getItemId() == R.id.main_nav_covid) {
                    currentFragment = new CovidFragment();
                } else if (menuItem.getItemId() == R.id.main_nav_again) {
                    Intent intentAgain = new Intent(getApplicationContext(), QuizActivity.class);
                    startActivity(intentAgain);
                    finish();
                }
                openFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        if (isBusiness == false) {
            Menu menuNav = navigationView.getMenu();
            MenuItem businessItem = menuNav.findItem(R.id.main_nav_business);
            businessItem.setVisible(false);
        }

    }

    private void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame_container, currentFragment).commit();
    }

    @Override
    public void notifyChanges(Map<String, List<Recommendation>> results) {
        recommendationMap.clear();
        recommendationMap.putAll(results);
    }

    private void readSharedPrefs() {
        answer2FoodAndDrinks = WelcomeActivity.preferences.getString("ANS2", "");
        answer3Outdoor = WelcomeActivity.preferences.getString("ANS3", "");
        answer4Museums = WelcomeActivity.preferences.getString("ANS4", "");
        answer5CoffeeAndBars = WelcomeActivity.preferences.getString("ANS5", "");
        answer6SportsAndSpa = WelcomeActivity.preferences.getString("ANS6", "");
        answer7Shopping = WelcomeActivity.preferences.getString("ANS7", "");
        answer8Casino = WelcomeActivity.preferences.getString("ANS8", "");
        answer9Nightlife = WelcomeActivity.preferences.getString("ANS9", "");
        answer10Transport = WelcomeActivity.preferences.getString("ANS10", "");
        answer11LocalGuide = WelcomeActivity.preferences.getString("ANS11", "");

        isBusiness = WelcomeActivity.preferences.getBoolean("ISBUSINESS", false);
    }

    private void navigateTo(String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
