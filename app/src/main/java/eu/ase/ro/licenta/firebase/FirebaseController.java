package eu.ase.ro.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class FirebaseController {
    private DatabaseReference database;
    private FirebaseDatabase controller;
    private FirebaseNotifier firebaseNotifier;
    private static FirebaseController firebaseController;

    private FirebaseController(FirebaseNotifier firebaseNotifier) {
        controller = FirebaseDatabase.getInstance();
        this.firebaseNotifier = firebaseNotifier;
    }

    private void open() {
        database = controller.getReference();
    }

    public static FirebaseController getInstance(FirebaseNotifier firebaseNotifier) {
        if (firebaseController == null) {
            synchronized (FirebaseController.class) {
                if (firebaseController == null) {
                    firebaseController = new FirebaseController(firebaseNotifier);
                }
            }
        }
        return firebaseController;
    }

    public void find() {
        open();
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, List<Recommendation>> recommendationMap = new HashMap<>();
                for (DataSnapshot category : dataSnapshot.getChildren()) {
                    for (DataSnapshot answer : category.getChildren()) {
                        List<Recommendation> recommendations = new ArrayList<>();
                        for (DataSnapshot data : answer.getChildren()) {
                            Recommendation r = data.getValue(Recommendation.class);
                            if (r != null) {
                                recommendations.add(r);
                            }
                        }
                        recommendationMap.put(answer.getKey(), recommendations);
                    }
                }
                firebaseNotifier.notifyChanges(recommendationMap);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Error while reading data");
            }
        });
    }
}