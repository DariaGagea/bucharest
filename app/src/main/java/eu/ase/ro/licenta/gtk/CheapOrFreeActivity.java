package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import eu.ase.ro.licenta.R;

public class CheapOrFreeActivity extends AppCompatActivity {

    private TextView tv1;
    private TextView tv2d;
    private TextView tv2e;
    private TextView tv2f;
    private TextView tv2g;
    private TextView tv2i;
    private TextView tv2j;
    private TextView tv2k;
    private TextView tv2l;
    private TextView tv2n;
    private TextView tv2o;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheap_or_free);
        initComponents();
    }

    private void initComponents() {
        tv1 = findViewById(R.id.activity_cheap_or_free_tv_1a);
        tv2d = findViewById(R.id.activity_cheap_or_free_tv_2d);
        tv2e = findViewById(R.id.activity_cheap_or_free_tv_2e);
        tv2f = findViewById(R.id.activity_cheap_or_free_tv_2f);
        tv2g = findViewById(R.id.activity_cheap_or_free_tv_2g);
        tv2i = findViewById(R.id.activity_cheap_or_free_tv_2i);
        tv2j = findViewById(R.id.activity_cheap_or_free_tv_2j);
        tv2k = findViewById(R.id.activity_cheap_or_free_tv_2k);
        tv2l = findViewById(R.id.activity_cheap_or_free_tv_2l);
        tv2n = findViewById(R.id.activity_cheap_or_free_tv_2n);
        tv2o = findViewById(R.id.activity_cheap_or_free_tv_2o);
        tv3 = findViewById(R.id.activity_cheap_or_free_tv_3a);
        tv4 = findViewById(R.id.activity_cheap_or_free_tv_4a);
        tv5 = findViewById(R.id.activity_cheap_or_free_tv_5a);
        tv6 = findViewById(R.id.activity_cheap_or_free_tv_6a);

        tv1.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link1)));
        tv2d.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2d)));
        tv2e.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2e)));
        tv2f.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2f)));
        tv2g.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2g)));
        tv2i.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2i)));
        tv2j.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2j)));
        tv2k.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2k)));
        tv2l.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2l)));
        tv2n.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2n)));
        tv2o.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link2o)));
        tv3.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link3)));
        tv4.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link4)));
        tv5.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link5)));
        tv6.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_cheap_or_free_link6)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
