package eu.ase.ro.licenta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class WelcomeActivity extends AppCompatActivity {
    public final static String SHARED_PREF_NAME = "sharedPrefName";
    public static SharedPreferences preferences;

    private RelativeLayout relativeLayout;
    private Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        preferences = getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        initComponents();
        initThread();
    }

    private void initComponents() {
        relativeLayout = findViewById(R.id.activity_welcome_rl1);

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thread.interrupt();
                thread = null;
                if (preferences.contains("ANS2")) {
                    Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intentMain);
                } else {
                    Intent intentChoose = new Intent(getApplicationContext(), ChooseActivity.class);
                    startActivity(intentChoose);
                }
                finish();
            }
        });
    }

    private void initThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    openWait();
                } catch (InterruptedException e) {
                    System.out.println("S-a dat click pe ecran");
                }
            }
        };
        thread = new Thread(runnable);
        thread.start();
    }

    private void openWait() {
        if (preferences.contains("ANS2")) {
            Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intentMain);
        } else {
            Intent intentChoose = new Intent(getApplicationContext(), ChooseActivity.class);
            startActivity(intentChoose);
        }
        finish();
    }
}
