package eu.ase.ro.licenta.gtk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.WelcomeActivity;
import eu.ase.ro.licenta.network.HTTPExchangeResponse;
import eu.ase.ro.licenta.network.HTTPManager;
import eu.ase.ro.licenta.network.JSONExchangeParser;

public class ATMActivity extends AppCompatActivity {

    private static final String DATE_FORMAT = "dd.MM.yyyy";
    private static final String URL = "https://api.exchangeratesapi.io/latest?base=RON&symbols=USD,EUR";

    private TextInputEditText tietLei;
    private TextInputEditText tietEur;
    private TextInputEditText tietUsd;
    private TextView tvEur;
    private TextView tvUsd;
    private TextView tvDate;
    private Boolean tietChangedByProgram = false;

    private HTTPExchangeResponse httpExchangeResponse;

    private Double eur;
    private Double usd;

    public static int permissionGranted;
    int locationRequestCode = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm);
        checkSharedPrefsPermission();

        setDate();

        new HTTPManager() {
            @Override
            protected void onPostExecute(String s) {
                httpExchangeResponse = JSONExchangeParser.parseJSON(s);
                if (httpExchangeResponse != null) {
                    eur = httpExchangeResponse.getRates().getEur();
                    usd = httpExchangeResponse.getRates().getUsd();

                    setTV();
                    setTextInputEditText();
                }
                super.onPostExecute(s);
            }
        }.execute(URL);
    }

    public void addFragmentAtm(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.activity_atm_ll_mapA, fragment);
        ft.commitAllowingStateLoss();
    }

    private void setDate() {
        tvDate = findViewById(R.id.activity_atm_tv_date);
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        String date = sdf.format(today);
        tvDate.setText(date);
    }

    private void setTextInputEditText() {
        tietLei = findViewById(R.id.activity_atm_tiet_lei);
        tietEur = findViewById(R.id.activity_atm_tiet_euro);
        tietUsd = findViewById(R.id.activity_atm_tiet_usd);

        tietLei.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double eurTV;
                double usdTV;
                if (tietChangedByProgram == false) {
                    tietChangedByProgram = true;
                    if (!s.toString().isEmpty()) {
                        eurTV = Double.valueOf(s.toString()) * eur; //ex: 50lei * 0.2.. = mai putini euro
                        tietEur.setText(String.format("%.4f", eurTV));
                        usdTV = Double.valueOf(s.toString()) * usd; //ex: 50lei * 0.23.. = mai putini dolari
                        tietUsd.setText(String.format("%.4f", usdTV));
                    } else {
                        tietEur.setText("");
                        tietUsd.setText("");
                    }
                    tietChangedByProgram = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tietEur.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double leiTV;
                double usdTV;
                if (tietChangedByProgram == false) {
                    tietChangedByProgram = true;
                    if (!s.toString().isEmpty()) {
                        leiTV = Double.valueOf(s.toString()) * 1 / eur; //ex: 50euro * 1/0.2.. = 50euro * 4.7..cursLei = mai multi lei
                        tietLei.setText(String.format("%.4f", leiTV));
                        usdTV = Double.valueOf(s.toString()) * 1 / eur * usd; //ex: 50euro * 1/0.2.. = 50euro * 4.7..cursLei=200..lei * 0.23.. = mai putini usd
                        tietUsd.setText(String.format("%.4f", usdTV));
                    } else {
                        tietLei.setText("");
                        tietUsd.setText("");
                    }
                    tietChangedByProgram = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tietUsd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double leiTV;
                double eurTV;
                if (tietChangedByProgram == false) {
                    tietChangedByProgram = true;
                    if (!s.toString().isEmpty()) {
                        leiTV = Double.valueOf(s.toString()) * 1 / usd; //ex: 50usd * 1/0.23..cursLei = 50usd * 4.5..curslei = mai multi lei
                        tietLei.setText(String.format("%.4f", leiTV));
                        eurTV = Double.valueOf(s.toString()) * 1 / usd * eur; //ex: 50usd * 1/0.23..cursLei = 50usd * 4.5..cursLei = mai multi lei * 0.2..euro = mai putini euro
                        tietEur.setText(String.format("%.4f", eurTV));
                    } else {
                        tietLei.setText("");
                        tietEur.setText("");
                    }
                    tietChangedByProgram = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setTV() {
        tvEur = findViewById(R.id.activity_atm_ll_tv2a);
        tvUsd = findViewById(R.id.activity_atm_ll_tv2b);

        tvEur.setText(String.format("%.4f", 1 / eur));
        tvUsd.setText(String.format("%.4f", 1 / usd));
    }

    private void checkSharedPrefsPermission() {
        permissionGranted = WelcomeActivity.preferences.getInt("PERMISSION", -1);
        if (permissionGranted == -1) { //0 = deny by default
            askForPermission();
        } else if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionGranted = 0;
            writeInSharedPrefsPermission();
            addFragmentAtm(new MapFragmentAtm());
        } else if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            permissionGranted = 1;
            writeInSharedPrefsPermission();
            addFragmentAtm(new MapFragmentAtm());
        }
    }

    private void askForPermission() {
        // reuqest for permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, locationRequestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                ///utilizatorul accepta preluarea locatiei
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = 1;
                } else { //utilizatorul nu accepta preluarea locatiei
                    permissionGranted = 0;
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
        writeInSharedPrefsPermission();
        addFragmentAtm(new MapFragmentAtm());
    }

    private void writeInSharedPrefsPermission() {
        SharedPreferences.Editor editor = WelcomeActivity.preferences.edit();
        editor.putInt("PERMISSION", permissionGranted);
        editor.apply();
    }
}
