package eu.ase.ro.licenta.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.rfy.CasinoActivity;
import eu.ase.ro.licenta.rfy.CoffeeAndBarsActivity;
import eu.ase.ro.licenta.rfy.FoodAndDrinksActivity;
import eu.ase.ro.licenta.rfy.LocalGuideActivity;
import eu.ase.ro.licenta.rfy.MuseumsActivity;
import eu.ase.ro.licenta.rfy.NightLifeActivity;
import eu.ase.ro.licenta.rfy.OutdoorActivity;
import eu.ase.ro.licenta.rfy.ShoppingActivity;
import eu.ase.ro.licenta.rfy.SportsAndSpaActivity;
import eu.ase.ro.licenta.rfy.TransportActivity;

public class RecommendedForYouFragment extends Fragment {

    private ImageView ivFoodAndDrinks;
    private ImageView ivOutdoor;
    private ImageView ivMuseums;
    private ImageView ivCoffeeAndBars;
    private ImageView ivSportsAndSpa;
    private ImageView ivShopping;
    private ImageView ivCasino;
    private ImageView ivNightlife;
    private ImageView ivTransport;
    private ImageView ivLocalGuide;

    public RecommendedForYouFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rfy, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(final View view) {
        ivFoodAndDrinks = view.findViewById(R.id.fragment_RFY_iv_food_and_drinks);
        ivOutdoor = view.findViewById(R.id.fragment_RFY_iv_outdoor);
        ivMuseums = view.findViewById(R.id.fragment_RFY_iv_museums);
        ivCoffeeAndBars = view.findViewById(R.id.fragment_RFY_iv_coffee_and_bars);
        ivSportsAndSpa = view.findViewById(R.id.fragment_RFY_iv_sports_and_spa);
        ivShopping = view.findViewById(R.id.fragment_RFY_iv_shopping);
        ivCasino = view.findViewById(R.id.fragment_RFY_iv_casino);
        ivNightlife = view.findViewById(R.id.fragment_RFY_iv_nightlife);
        ivTransport = view.findViewById(R.id.fragment_RFY_iv_transport);
        ivLocalGuide = view.findViewById(R.id.fragment_RFY_iv_local_guide);

        ivFoodAndDrinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentFAD = new Intent(view.getContext(), FoodAndDrinksActivity.class);
                startActivity(intentFAD);
            }
        });

        ivOutdoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentOut = new Intent(view.getContext(), OutdoorActivity.class);
                startActivity(intentOut);
            }
        });

        ivMuseums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentM = new Intent(view.getContext(), MuseumsActivity.class);
                startActivity(intentM);
            }
        });

        ivCoffeeAndBars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCAB = new Intent(view.getContext(), CoffeeAndBarsActivity.class);
                startActivity(intentCAB);
            }
        });

        ivSportsAndSpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSAS = new Intent(view.getContext(), SportsAndSpaActivity.class);
                startActivity(intentSAS);
            }
        });

        ivShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentShp = new Intent(view.getContext(), ShoppingActivity.class);
                startActivity(intentShp);
            }
        });

        ivCasino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCas = new Intent(view.getContext(), CasinoActivity.class);
                startActivity(intentCas);
            }
        });

        ivNightlife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNight = new Intent(view.getContext(), NightLifeActivity.class);
                startActivity(intentNight);
            }
        });

        ivTransport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTr = new Intent(view.getContext(), TransportActivity.class);
                startActivity(intentTr);
            }
        });

        ivLocalGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLG = new Intent(view.getContext(), LocalGuideActivity.class);
                startActivity(intentLG);
            }
        });
    }
}