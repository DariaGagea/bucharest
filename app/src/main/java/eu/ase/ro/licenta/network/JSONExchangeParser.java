package eu.ase.ro.licenta.network;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONExchangeParser {
    public static HTTPExchangeResponse parseJSON(String json) {
        if (json == null) {
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            Rates rates = getRatesObject(obj.getJSONObject("rates"));
            String base = obj.getString("base");
            String date = obj.getString("date");
            return new HTTPExchangeResponse(rates, base, date);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Rates getRatesObject(JSONObject obj) throws JSONException {
        if (obj == null) {
            return null;
        }
        Double eur = obj.getDouble("EUR");
        Double usd = obj.getDouble("USD");
        return new Rates(eur, usd);
    }
}
