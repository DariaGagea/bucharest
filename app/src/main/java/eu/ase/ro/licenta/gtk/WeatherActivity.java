package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androdocs.httprequest.HttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import eu.ase.ro.licenta.R;

public class WeatherActivity extends AppCompatActivity {

    private String CITY = "bucharest,ro";
    private String API = "b0d2c753a2c152f0e9cc63a81d3020f7";

    private TextView tvAddress;
    private TextView tvUpdatedAt;
    private TextView tvStatus;
    private TextView tvTemp;
    private TextView tvTemp_min;
    private TextView tvTemp_max;
    private TextView tvSunrise;
    private TextView tvSunset;
    private TextView tvWind;
    private TextView tvPressure;
    private TextView tvHumidity;

    private LinearLayout llbttt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        initComponents();
        new weatherTask().execute();
    }

    private void initComponents() {
        tvAddress = findViewById(R.id.address);
        tvUpdatedAt = findViewById(R.id.updated_at);
        tvStatus = findViewById(R.id.status);
        tvTemp = findViewById(R.id.temp);
        tvTemp_min = findViewById(R.id.temp_min);
        tvTemp_max = findViewById(R.id.temp_max);
        tvSunrise = findViewById(R.id.sunrise);
        tvSunset = findViewById(R.id.sunset);
        tvWind = findViewById(R.id.wind);
        tvPressure = findViewById(R.id.pressure);
        tvHumidity = findViewById(R.id.humidity);

        llbttt = findViewById(R.id.activity_weather_ll_bttt);
        llbttt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBTTT = new Intent(getApplicationContext(), BestTimeToTravelActivity.class);
                startActivity(intentBTTT);
            }
        });
    }

    class weatherTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            /* Showing the ProgressBar, Making the main design GONE */
            findViewById(R.id.loader).setVisibility(View.VISIBLE);
            findViewById(R.id.activity_weather_main_container).setVisibility(View.GONE);
            findViewById(R.id.errorText).setVisibility(View.GONE);
        }

        protected String doInBackground(String... args) {
            String response = HttpRequest.excuteGet("https://api.openweathermap.org/data/2.5/weather?q=" + CITY + "&units=metric&appid=" + API);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObj = new JSONObject(result);
                JSONObject main = jsonObj.getJSONObject("main");
                JSONObject sys = jsonObj.getJSONObject("sys");
                JSONObject wind = jsonObj.getJSONObject("wind");
                JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);

                Long updatedAt = jsonObj.getLong("dt");
                String updatedAtText = "Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000));
                String temp = main.getString("temp") + "°C";
                String tempMin = "Min Temp: " + main.getString("temp_min") + "°C";
                String tempMax = "Max Temp: " + main.getString("temp_max") + "°C";
                String pressure = main.getString("pressure");
                String humidity = main.getString("humidity");

                Long sunrise = sys.getLong("sunrise");
                Long sunset = sys.getLong("sunset");
                String windSpeed = wind.getString("speed");
                String weatherDescription = weather.getString("description");

                String address = jsonObj.getString("name") + ", " + sys.getString("country");

                tvAddress.setText(address);
                tvUpdatedAt.setText(updatedAtText);
                tvStatus.setText(weatherDescription.toUpperCase());
                tvTemp.setText(temp);
                tvTemp_min.setText(tempMin);
                tvTemp_max.setText(tempMax);
                tvSunrise.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunrise * 1000)));
                tvSunset.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunset * 1000)));
                tvWind.setText(windSpeed);
                tvPressure.setText(pressure);
                tvHumidity.setText(humidity);

                /* Views populated, Hiding the loader, Showing the main design */
                findViewById(R.id.loader).setVisibility(View.GONE);
                findViewById(R.id.activity_weather_main_container).setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                findViewById(R.id.loader).setVisibility(View.GONE);
                findViewById(R.id.errorText).setVisibility(View.VISIBLE);
            }
        }
    }
}
