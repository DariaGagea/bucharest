package eu.ase.ro.licenta.firebase;

public class Recommendation {

    private String name;
    private String imageId;
    private String link;

    public Recommendation() {

    }

    public Recommendation(String name, String imageId, String link) {
        this.name = name;
        this.imageId = imageId;
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Recommendation{" +
                "name='" + name + '\'' +
                ", imageId='" + imageId + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
