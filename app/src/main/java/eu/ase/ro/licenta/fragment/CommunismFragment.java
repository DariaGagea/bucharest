package eu.ase.ro.licenta.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import eu.ase.ro.licenta.R;

public class CommunismFragment extends Fragment {

    private ImageView iv;

    public CommunismFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_communism, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(View view) {
        iv = view.findViewById(R.id.fragment_communism_iv);
        iv.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_communism_link1)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
