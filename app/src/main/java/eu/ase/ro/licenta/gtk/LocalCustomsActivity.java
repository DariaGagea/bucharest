package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import eu.ase.ro.licenta.R;

public class LocalCustomsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_customs);
    }
}
