package eu.ase.ro.licenta.network;

public class HTTPExchangeResponse {
    Rates rates;
    String base;
    String date;

    public HTTPExchangeResponse(Rates rates, String base, String date) {
        this.rates = rates;
        this.base = base;
        this.date = date;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "HTTPExchangeResponse{" +
                "rates=" + rates +
                ", base='" + base + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
