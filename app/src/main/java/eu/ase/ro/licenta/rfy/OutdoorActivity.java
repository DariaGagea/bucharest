package eu.ase.ro.licenta.rfy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.licenta.MainActivity;
import eu.ase.ro.licenta.QuizActivity;
import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.firebase.FirebaseController;
import eu.ase.ro.licenta.firebase.FirebaseNotifier;
import eu.ase.ro.licenta.firebase.Recommendation;

public class OutdoorActivity extends AppCompatActivity {
    public static final String DRAWABLE = "@drawable/";

    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;
    private ImageView iv6;

    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;

    private List<Recommendation> recommendations = new ArrayList<>();

    private ConstraintLayout cl2;
    private ConstraintLayout cl3;
    private ConstraintLayout cl4;
    private ConstraintLayout cl5;
    private ConstraintLayout cl6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outdoor);
        initComponents();
        recommendations = MainActivity.recommendationMap.get(MainActivity.answer3Outdoor);
        initFields();
    }

    private void initComponents() {
        iv1 = findViewById(R.id.activity_outdoor_iv_1);
        iv2 = findViewById(R.id.activity_outdoor_iv_2);
        iv3 = findViewById(R.id.activity_outdoor_iv_3);
        iv4 = findViewById(R.id.activity_outdoor_iv_4);
        iv5 = findViewById(R.id.activity_outdoor_iv_5);
        iv6 = findViewById(R.id.activity_outdoor_iv_6);

        tv1 = findViewById(R.id.activity_outdoor_tv_1);
        tv2 = findViewById(R.id.activity_outdoor_tv_2);
        tv3 = findViewById(R.id.activity_outdoor_tv_3);
        tv4 = findViewById(R.id.activity_outdoor_tv_4);
        tv5 = findViewById(R.id.activity_outdoor_tv_5);
        tv6 = findViewById(R.id.activity_outdoor_tv_6);
    }

    private void initFields() {
        if (MainActivity.answer3Outdoor.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_outdoorVar2)) ||
                MainActivity.answer3Outdoor.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_outdoorVar3))
        ) {
            tv1.setText(recommendations.get(0).getName());
            iv1.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(0).getImageId(), null, getPackageName()));

            tv2.setText(recommendations.get(1).getName());
            iv2.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(1).getImageId(), null, getPackageName()));

            tv3.setText(recommendations.get(2).getName());
            iv3.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(2).getImageId(), null, getPackageName()));

            tv4.setText(recommendations.get(3).getName());
            iv4.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(3).getImageId(), null, getPackageName()));

            tv5.setText(recommendations.get(4).getName());
            iv5.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(4).getImageId(), null, getPackageName()));

            cl6 = findViewById(R.id.activity_outdoor_cl6);
            cl6.setVisibility(View.GONE);

            iv1.setOnClickListener(v -> navigateTo(v, recommendations.get(0).getLink()));
            iv2.setOnClickListener(v -> navigateTo(v, recommendations.get(1).getLink()));
            iv3.setOnClickListener(v -> navigateTo(v, recommendations.get(2).getLink()));
            iv4.setOnClickListener(v -> navigateTo(v, recommendations.get(3).getLink()));
            iv5.setOnClickListener(v -> navigateTo(v, recommendations.get(4).getLink()));

        } else if (MainActivity.answer3Outdoor.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_outdoorVar4))) {
            tv1.setText(recommendations.get(0).getName());
            iv1.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(0).getImageId(), null, getPackageName()));

            tv2.setText(recommendations.get(1).getName());
            iv2.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(1).getImageId(), null, getPackageName()));

            cl3 = findViewById(R.id.activity_outdoor_cl3);
            cl4 = findViewById(R.id.activity_outdoor_cl4);
            cl5 = findViewById(R.id.activity_outdoor_cl5);
            cl6 = findViewById(R.id.activity_outdoor_cl6);

            cl3.setVisibility(View.GONE);
            cl4.setVisibility(View.GONE);
            cl5.setVisibility(View.GONE);
            cl6.setVisibility(View.GONE);

            iv1.setOnClickListener(v -> navigateTo(v, recommendations.get(0).getLink()));
            iv2.setOnClickListener(v -> navigateTo(v, recommendations.get(1).getLink()));
        } else {
            tv1.setText(recommendations.get(0).getName());
            iv1.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(0).getImageId(), null, getPackageName()));

            tv2.setText(recommendations.get(1).getName());
            iv2.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(1).getImageId(), null, getPackageName()));

            tv3.setText(recommendations.get(2).getName());
            iv3.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(2).getImageId(), null, getPackageName()));

            tv4.setText(recommendations.get(3).getName());
            iv4.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(3).getImageId(), null, getPackageName()));

            tv5.setText(recommendations.get(4).getName());
            iv5.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(4).getImageId(), null, getPackageName()));

            tv6.setText(recommendations.get(5).getName());
            iv6.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(5).getImageId(), null, getPackageName()));

            iv1.setOnClickListener(v -> navigateTo(v, recommendations.get(0).getLink()));
            iv2.setOnClickListener(v -> navigateTo(v, recommendations.get(1).getLink()));
            iv3.setOnClickListener(v -> navigateTo(v, recommendations.get(2).getLink()));
            iv4.setOnClickListener(v -> navigateTo(v, recommendations.get(3).getLink()));
            iv5.setOnClickListener(v -> navigateTo(v, recommendations.get(4).getLink()));
            iv6.setOnClickListener(v -> navigateTo(v, recommendations.get(5).getLink()));
        }
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}