package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import eu.ase.ro.licenta.R;

public class KidsActivity extends AppCompatActivity {

    private ImageView iv1;
    private ImageView iv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kids);
        initComponents();
    }

    private void initComponents() {
        iv1 = findViewById(R.id.activity_kids_iv_1);
        iv2 = findViewById(R.id.activity_kids_iv_2);

        iv1.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_kids_link_nannyexpress)));
        iv2.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_kids_link_ileanadobre)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}