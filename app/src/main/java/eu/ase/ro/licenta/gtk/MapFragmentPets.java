package eu.ase.ro.licenta.gtk;

import android.Manifest;
import android.content.Context;
import android.content.PeriodicSync;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import eu.ase.ro.licenta.R;

public class MapFragmentPets extends Fragment {

    private FusedLocationProviderClient mFusedLocationClient;

    private double wayLatitude = 0.0;
    private double wayLongitude = 0.0;

    public MapFragmentPets() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_pets, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_pets);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                if (PetsActivity.permissionGranted == 1) {
                    getDeviceLocation();
                    mMap.setMyLocationEnabled(true);
                }
                mMap.clear();

                CameraPosition googlePlex = CameraPosition.builder()
                        .target(new LatLng(44.439663, 26.096306))
                        .zoom(10)
                        .bearing(0)
                        .tilt(45)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10000, null);

                //cabinete veterinare - ok si pentru farmacie veterinara & ingrijire
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.447721, 26.069458))
                        .title(getString(R.string.map_fragment_pets_VOP))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.432646, 26.081131))
                        .title(getString(R.string.map_fragment_pets_CFVM))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.427007, 26.060360))
                        .title(getString(R.string.map_fragment_pets_BVC))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.418057, 26.067226))
                        .title(getString(R.string.map_fragment_pets_SV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.420409, 26.104743))
                        .title(getString(R.string.map_fragment_pets_SOSV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.414431, 26.159319))
                        .title(getString(R.string.map_fragment_pets_IVH))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.419091, 26.174426))
                        .title(getString(R.string.map_fragment_pets_HV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.428163, 26.132883))
                        .title(getString(R.string.map_fragment_pets_CV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.448019, 26.147646))
                        .title(getString(R.string.map_fragment_pets_CRV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.460948, 26.135683))
                        .title(getString(R.string.map_fragment_pets_MV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.497262, 26.119552))
                        .title(getString(R.string.map_fragment_pets_MV))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                //petshop
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.467587, 26.067248))
                        .title(getString(R.string.map_fragment_pets_animax))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.416059, 26.139106))
                        .title(getString(R.string.map_fragment_pets_animax))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.411477, 26.139928))
                        .title(getString(R.string.map_fragment_pets_animax))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.411773, 26.203925))
                        .title(getString(R.string.map_fragment_pets_animax))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.424939, 26.150698))
                        .title(getString(R.string.map_fragment_pets_animax))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.441943, 26.153115))
                        .title(getString(R.string.map_fragment_pets_zoomania))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.461742, 26.128877))
                        .title(getString(R.string.map_fragment_pets_petmania))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
            }
        });
        return rootView;
    }

    // already permission granted
    private void getDeviceLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                    }
                }
            }
        };

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
            if (location != null) {
                wayLatitude = location.getLatitude();
                wayLongitude = location.getLongitude();
            } else {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            }
        });
    }
}