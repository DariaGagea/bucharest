package eu.ase.ro.licenta.firebase;

import java.util.List;
import java.util.Map;

public interface FirebaseNotifier {

    void notifyChanges(Map<String, List<Recommendation>> results);
}
