package eu.ase.ro.licenta.gtk;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import eu.ase.ro.licenta.R;

public class MapFragmentPharma extends Fragment {

    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0;
    private double wayLongitude = 0.0;

    public MapFragmentPharma() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_pharma, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_pharma);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                if (MedicalEmergencyActivity.permissionGranted == 1) {
                    getDeviceLocation();
                    mMap.setMyLocationEnabled(true);
                }
                mMap.clear();

                CameraPosition googlePlex = CameraPosition.builder()
                        .target(new LatLng(44.439663, 26.096306))
                        .zoom(10)
                        .bearing(0)
                        .tilt(45)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10000, null);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.515705, 26.115707))
                        .title(getString(R.string.map_fragment_pharma_ameris_pharma))
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.511666, 26.086696))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.511430, 26.089142))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.508370, 26.137894))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.495759, 26.091374))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.493310, 26.079872))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.492331, 26.127251))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.492086, 26.133259))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.485718, 26.072319))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.490861, 26.183727))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.494045, 26.190766))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.472815, 26.052579))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.461849, 26.075628))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.463169, 26.095405))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.461545, 26.100101))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.467536, 26.104653))
                        .title(getString(R.string.map_fragment_pharma_farmadex)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.461139, 26.113902))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.463982, 26.119308))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.459514, 26.047030))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.429472, 26.034530))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.427937, 26.052660))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.425252, 26.071998))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.425557, 26.074684))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.437821, 26.095100))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.430417, 26.099960))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.429260, 26.104983))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.435276, 26.120700))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.426982, 26.051423))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.421118, 26.018386))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.423212, 26.064325))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.407294, 26.095016))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.393118, 26.111437))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.395702, 26.122775))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.421095, 26.126277))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.415789, 26.133216))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.414253, 26.142013))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.409855, 26.139863))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.419769, 26.143088))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.422561, 26.163517))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.415021, 26.164494))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.409436, 26.171434))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.416837, 26.175539))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.426498, 26.164534))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.426294, 26.144599))
                        .title(getString(R.string.map_fragment_pharma_tinos)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.429553, 26.147604))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.421365, 26.136918))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.425221, 26.136807))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.426771, 26.137030))
                        .title(getString(R.string.map_fragment_pharma_belladona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.429633, 26.132911))
                        .title(getString(R.string.map_fragment_pharma_decebal)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.431103, 26.135805))
                        .title(getString(R.string.map_fragment_pharma_farmadex)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.433130, 26.130240))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.438574, 26.139367))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.442613, 26.137708))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.444291, 26.157114))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.439533, 26.108114))
                        .title(getString(R.string.map_fragment_pharma_help_net)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.444739, 26.110949))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.453876, 26.114028))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.451737, 26.124641))
                        .title(getString(R.string.map_fragment_pharma_dona)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.445201, 26.129015))
                        .title(getString(R.string.map_fragment_pharma_catena)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.463013, 26.118565))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.444681, 26.157208))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.479020, 26.116001))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.480421, 26.103827))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.483665, 26.091857))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(44.388140, 26.109586))
                        .title(getString(R.string.map_fragment_pharma_sensiblu)).icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
            }
        });
        return rootView;
    }

    // already permission granted
    private void getDeviceLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                    }
                }
            }
        };

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
            if (location != null) {
                wayLatitude = location.getLatitude();
                wayLongitude = location.getLongitude();
            } else {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            }
        });
    }
}
