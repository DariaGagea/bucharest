package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import eu.ase.ro.licenta.R;

public class HairstylingActivity extends AppCompatActivity {

    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;
    private ImageView iv6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hairstyling);
        initComponents();
    }

    private void initComponents() {
        iv1 = findViewById(R.id.activity_hairstyling_iv_1);
        iv2 = findViewById(R.id.activity_hairstyling_iv_2);
        iv3 = findViewById(R.id.activity_hairstyling_iv_3);
        iv4 = findViewById(R.id.activity_hairstyling_iv_4);
        iv5 = findViewById(R.id.activity_hairstyling_iv_5);
        iv6 = findViewById(R.id.activity_hairstyling_iv_6);

        iv1.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_hairstyling_link_geta_voinea)));
        iv2.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_hairstyling_link_beauty_district)));
        iv3.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_hairstyling_link_biotiful)));
        iv4.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_hairstyling_link_encore)));
        iv5.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_hairstyling_link_jovsky)));
        iv6.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_hairstyling_link_michelle)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
