package eu.ase.ro.licenta.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import eu.ase.ro.licenta.R;

public class RomaniaFragment extends Fragment {
    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;

    public RomaniaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_romania, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(final View view) {
        iv1 = view.findViewById(R.id.fragment_romania_iv_1);
        iv2 = view.findViewById(R.id.fragment_romania_iv_2);
        iv3 = view.findViewById(R.id.fragment_romania_iv_3);
        iv4 = view.findViewById(R.id.fragment_romania_iv_4);
        iv5 = view.findViewById(R.id.fragment_romania_iv_5);

        iv1.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_romania_link1)));
        iv2.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_romania_link2)));
        iv3.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_romania_link3)));
        iv4.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_romania_link4)));
        iv5.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_romania_link5)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
