package eu.ase.ro.licenta.rfy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.licenta.MainActivity;
import eu.ase.ro.licenta.QuizActivity;
import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.firebase.FirebaseController;
import eu.ase.ro.licenta.firebase.FirebaseNotifier;
import eu.ase.ro.licenta.firebase.Recommendation;

public class ShoppingActivity extends AppCompatActivity {
    public static final String DRAWABLE = "@drawable/";

    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;
    private ImageView iv6;

    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;

    private TextView tvTxt;

    private List<Recommendation> recommendations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);
        initComponents();
        recommendations = MainActivity.recommendationMap.get(MainActivity.answer7Shopping);
        initFields();
    }

    private void initComponents() {
        iv1 = findViewById(R.id.activity_shopping_iv_1);
        iv2 = findViewById(R.id.activity_shopping_iv_2);
        iv3 = findViewById(R.id.activity_shopping_iv_3);
        iv4 = findViewById(R.id.activity_shopping_iv_4);
        iv5 = findViewById(R.id.activity_shopping_iv_5);
        iv6 = findViewById(R.id.activity_shopping_iv_6);

        tv1 = findViewById(R.id.activity_shopping_tv_1);
        tv2 = findViewById(R.id.activity_shopping_tv_2);
        tv3 = findViewById(R.id.activity_shopping_tv_3);
        tv4 = findViewById(R.id.activity_shopping_tv_4);
        tv5 = findViewById(R.id.activity_shopping_tv_5);
        tv6 = findViewById(R.id.activity_shopping_tv_6);
    }

    private void initFields() {
        if (MainActivity.answer7Shopping.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_shoppingVar1)) ||
                MainActivity.answer7Shopping.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_shoppingVar2)) ||
                MainActivity.answer7Shopping.equalsIgnoreCase(getResources().getString(R.string.activity_quiz_shoppingVar3))) {
            tvTxt = findViewById(R.id.activity_shopping_tv_text);
            tvTxt.setVisibility(View.GONE);
        }

        tv1.setText(recommendations.get(0).getName());
        iv1.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(0).getImageId(), null, getPackageName()));
        iv1.setOnClickListener(v -> navigateTo(v, recommendations.get(0).getLink()));

        tv2.setText(recommendations.get(1).getName());
        iv2.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(1).getImageId(), null, getPackageName()));
        iv2.setOnClickListener(v -> navigateTo(v, recommendations.get(1).getLink()));

        tv3.setText(recommendations.get(2).getName());
        iv3.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(2).getImageId(), null, getPackageName()));
        iv3.setOnClickListener(v -> navigateTo(v, recommendations.get(2).getLink()));

        tv4.setText(recommendations.get(3).getName());
        iv4.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(3).getImageId(), null, getPackageName()));
        iv4.setOnClickListener(v -> navigateTo(v, recommendations.get(3).getLink()));

        tv5.setText(recommendations.get(4).getName());
        iv5.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(4).getImageId(), null, getPackageName()));
        iv5.setOnClickListener(v -> navigateTo(v, recommendations.get(4).getLink()));

        tv6.setText(recommendations.get(5).getName());
        iv6.setImageResource(getResources().getIdentifier(DRAWABLE + recommendations.get(5).getImageId(), null, getPackageName()));
        iv6.setOnClickListener(v -> navigateTo(v, recommendations.get(5).getLink()));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
