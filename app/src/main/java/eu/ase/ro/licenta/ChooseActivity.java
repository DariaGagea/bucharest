package eu.ase.ro.licenta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ChooseActivity extends AppCompatActivity {

    private Button btnStartLeisure;
    private Button btnStartBusiness;

    private Boolean isBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        initComponents();
    }

    private void initComponents() {
        btnStartLeisure = findViewById(R.id.activity_choose_btn_lt);
        btnStartBusiness = findViewById(R.id.activity_choose_btn_bt);

        btnStartLeisure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBusiness = false;
                SharedPreferences.Editor editor = WelcomeActivity.preferences.edit();
                editor.putBoolean("ISBUSINESS", isBusiness);
                editor.apply();

                Intent intentQuiz = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(intentQuiz);
            }
        });

        btnStartBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBusiness = true;
                SharedPreferences.Editor editor = WelcomeActivity.preferences.edit();
                editor.putBoolean("ISBUSINESS", isBusiness);
                editor.apply();

                Intent intentQuiz = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(intentQuiz);
            }
        });
    }
}
