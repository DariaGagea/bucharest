package eu.ase.ro.licenta.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import eu.ase.ro.licenta.gtk.ATMActivity;
import eu.ase.ro.licenta.gtk.CheapOrFreeActivity;
import eu.ase.ro.licenta.gtk.HairstylingActivity;
import eu.ase.ro.licenta.gtk.KidsActivity;
import eu.ase.ro.licenta.gtk.LocalCustomsActivity;
import eu.ase.ro.licenta.gtk.MedicalEmergencyActivity;
import eu.ase.ro.licenta.gtk.PetsActivity;
import eu.ase.ro.licenta.gtk.SafetyActivity;
import eu.ase.ro.licenta.gtk.TrafficActivity;
import eu.ase.ro.licenta.gtk.WeatherActivity;
import eu.ase.ro.licenta.R;

public class GoodToKnowFragment extends Fragment {

    private ImageView ivMedicalEmergency;
    private ImageView ivHairstyling;
    private ImageView ivKids;
    private ImageView ivPets;
    private ImageView ivWeather;
    private ImageView ivSafety;
    private ImageView ivTraffic;
    private ImageView ivLocalCustoms;
    private ImageView ivATM;
    private ImageView ivFreeOrCheap;

    public GoodToKnowFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gtk, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(View view) {
        ivMedicalEmergency = view.findViewById(R.id.fragment_GTK_iv_medicalEmergency);
        ivHairstyling = view.findViewById(R.id.fragment_GTK_iv_hairstyling);
        ivKids = view.findViewById(R.id.fragment_GTK_iv_kids);
        ivPets = view.findViewById(R.id.fragment_GTK_iv_pets);
        ivWeather = view.findViewById(R.id.fragment_GTK_iv_weather);
        ivSafety = view.findViewById(R.id.fragment_GTK_iv_safety);
        ivTraffic = view.findViewById(R.id.fragment_GTK_iv_traffic);
        ivLocalCustoms = view.findViewById(R.id.fragment_GTK_iv_local_customs);
        ivATM = view.findViewById(R.id.fragment_GTK_iv_atm);
        ivFreeOrCheap = view.findViewById(R.id.fragment_GTK_iv_cheap_or_free);

        ivMedicalEmergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMedical = new Intent(view.getContext(), MedicalEmergencyActivity.class);
                startActivity(intentMedical);
            }
        });

        ivHairstyling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHairstyling = new Intent(view.getContext(), HairstylingActivity.class);
                startActivity(intentHairstyling);
            }
        });

        ivKids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentKids = new Intent(view.getContext(), KidsActivity.class);
                startActivity(intentKids);
            }
        });

        ivPets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPets = new Intent(view.getContext(), PetsActivity.class);
                startActivity(intentPets);
            }
        });

        ivWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentWeather = new Intent(view.getContext(), WeatherActivity.class);
                startActivity(intentWeather);
            }
        });

        ivSafety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSafety = new Intent(view.getContext(), SafetyActivity.class);
                startActivity(intentSafety);
            }
        });

        ivTraffic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTraffic = new Intent(view.getContext(), TrafficActivity.class);
                startActivity(intentTraffic);
            }
        });

        ivLocalCustoms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLocalCustoms = new Intent(view.getContext(), LocalCustomsActivity.class);
                startActivity(intentLocalCustoms);
            }
        });

        ivATM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentATM = new Intent(view.getContext(), ATMActivity.class);
                startActivity(intentATM);
            }
        });

        ivFreeOrCheap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCheapOrFree = new Intent(view.getContext(), CheapOrFreeActivity.class);
                startActivity(intentCheapOrFree);
            }
        });
    }
}
