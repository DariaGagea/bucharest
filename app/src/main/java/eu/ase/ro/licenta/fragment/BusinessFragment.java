package eu.ase.ro.licenta.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import eu.ase.ro.licenta.R;

public class BusinessFragment extends Fragment {

    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv3;
    private ImageView iv4;
    private ImageView iv5;

    public BusinessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_business, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(View view) {
        iv1 = view.findViewById(R.id.fragment_business_iv_expat_solutions);
        iv2 = view.findViewById(R.id.fragment_business_iv_nestlers);
        iv3 = view.findViewById(R.id.fragment_business_iv_dbi);
        iv4 = view.findViewById(R.id.fragment_business_iv_ccib);
        iv5 = view.findViewById(R.id.fragment_business_iv_fic);

        iv1.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_business_link_expat_solutions)));
        iv2.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_business_link_nestlers)));
        iv3.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_business_link_dbi)));
        iv4.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_business_link_ccib)));
        iv5.setOnClickListener(v -> navigateTo(v, getString(R.string.fragment_business_link_fic)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
