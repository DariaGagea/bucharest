package eu.ase.ro.licenta.network;

public class Rates {
    Double eur;
    Double usd;

    public Rates(Double eur, Double usd) {
        this.eur = eur;
        this.usd = usd;
    }

    public Double getEur() {
        return eur;
    }

    public void setEur(Double eur) {
        this.eur = eur;
    }

    public Double getUsd() {
        return usd;
    }

    public void setUsd(Double usd) {
        this.usd = usd;
    }

    @Override
    public String toString() {
        return "Rates{" +
                "eur='" + eur + '\'' +
                ", usd='" + usd + '\'' +
                '}';
    }
}
