package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import eu.ase.ro.licenta.R;

public class TrafficActivity extends AppCompatActivity {

    private Button btnClick1;
    private Button btnClick2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic);
        initComponents();
    }

    private void initComponents() {
        btnClick1 = findViewById(R.id.activity_traffic_btn_1);
        btnClick2 = findViewById(R.id.activity_traffic_btn_2);

        btnClick1.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_traffic_link_1)));

        btnClick2.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_traffic_link_2)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
