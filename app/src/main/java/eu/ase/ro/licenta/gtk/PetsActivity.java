package eu.ase.ro.licenta.gtk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import eu.ase.ro.licenta.R;
import eu.ase.ro.licenta.WelcomeActivity;

public class PetsActivity extends AppCompatActivity {

    public static int permissionGranted;
    int locationRequestCode = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pets);
        checkSharedPrefsPermission();
    }

    public void addFragmentPets(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.activity_pets_ll_mapP, fragment);
        ft.commitAllowingStateLoss();
    }

    private void checkSharedPrefsPermission() {
        permissionGranted = WelcomeActivity.preferences.getInt("PERMISSION", -1);
        if (permissionGranted == -1) { //0 = deny by default
            askForPermission();
        } else if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionGranted = 0;
            writeInSharedPrefsPermission();
            addFragmentPets(new MapFragmentPets());
        } else if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            permissionGranted = 1;
            writeInSharedPrefsPermission();
            addFragmentPets(new MapFragmentPets());
        }
    }

    private void askForPermission() {
        // reuqest for permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, locationRequestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                ///utilizatorul accepta preluarea locatiei
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = 1;
                } else { //utilizatorul nu accepta preluarea locatiei
                    permissionGranted = 0;
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
        writeInSharedPrefsPermission();
        addFragmentPets(new MapFragmentPets());
    }

    private void writeInSharedPrefsPermission() {
        SharedPreferences.Editor editor = WelcomeActivity.preferences.edit();
        editor.putInt("PERMISSION", permissionGranted);
        editor.apply();
    }
}
