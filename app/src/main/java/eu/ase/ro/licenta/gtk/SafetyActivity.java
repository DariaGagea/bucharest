package eu.ase.ro.licenta.gtk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import eu.ase.ro.licenta.R;

public class SafetyActivity extends AppCompatActivity {

    private TextView tvClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safety);
        initComponents();
    }

    private void initComponents() {
        tvClick = findViewById(R.id.activity_safety_tv_click1);
        tvClick.setOnClickListener(v -> navigateTo(v, getString(R.string.activity_safety_link)));
    }

    private void navigateTo(View v, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(intent);
    }
}
